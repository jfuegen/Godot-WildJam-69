# Godot WildJam 69

### Arbeitsaufteilung

Roman - Journal,Cauldron\
Dome - Void\
Enes - Player Gathering, Item Partikel, Screen Overlay
Jonas - EndScreen, Shaders/Transitions

## Current TODO

Player Assets\
Sounds\
Transitions zwischen Void Stage changes\
Musik\
Animated Assets für Map (Bäume etc.)\
Partikel für Items auf Map\
UI VFX (Button hover effect etc.)\
Player Gathering mechanic (Muss in Area von Item sein und 1sec lang E drücken, Balken mit Progress)\
Code Refactoring / Schnittstellen überarbeiten (Clean Code)\
Screen Overlay effects (Schwarze Edges wenn Zeit knapp wird, Opcaity/Saturation change je nach Void Stage) etc.

### Scene Structure
```
Game
│   ~~Item Ressource~~
│   ~~Globals~~
│   Assets
│   Sounds 
│
└───Player
│
└───UI Manager
│      ~~StartScreen~~
│      ~~EndScreen~~
│      ~~Inventory~~
│      Journal
│      ~~PauseMenu~~
│   
└───MapManager
│      ~~Map~~
│      ~~RessourceNodes~~
│      Void
│      Cauldron
│
└───ItemManager
<<<<<<< HEAD
       ~~Item~~
```
=======
	   Item
```
>>>>>>> dev
