extends Control

func _ready():
	hide()

func _on_yes_button_pressed():
	SignalBus.entering_map.emit()
	hide()

func _on_no_button_pressed():
	hide()

func trigger():
	if visible:
		hide()
	else:
		visible = true
