extends Node
class_name LevelManager

@onready var map = preload("res://map/map.tscn").instantiate()
@onready var void_entity = preload("res://entity/void.tscn").instantiate()
@onready var cauldron = preload("res://entity/cauldron.tscn").instantiate()
@onready var player = preload("res://entity/player/player.tscn").instantiate()

func _ready():
	add_child(map)

func create_and_distribute_item_nodes(items: Array):
	var nodes = []
	for item in items:
		var item_node = load("res://entity/item_node.tscn").instantiate()
		item_node.set_properties(item)
		nodes.append(item_node)
	nodes.shuffle()
	map.distribute_item_nodes(nodes)

func remove_item_nodes():
	map.remove_item_nodes()
	
func init_all_entities():
	map.init_player(player)
	map.init_void_entity(void_entity)
	map.init_cauldron(cauldron)

