extends Control

func trigger():
	if visible:
		hide()
	else:
		visible = true
		
func _on_previous_stage_pressed():
	SignalBus.decrease_stage.emit()

func _on_next_stage_pressed():
	SignalBus.increase_stage.emit()

func _on_speed_pressed():
	pass

func _on_give_three_items_pressed():
	pass

