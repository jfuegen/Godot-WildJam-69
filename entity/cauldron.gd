extends StaticBody2D

var player_in_area = null

@onready var animation = $CauldronAnimation
@onready var cauldron_timer = $CauldronTimer
@onready var cauldron_progress_bar = $CauldronProgressBar
var first_potion = true

func _ready():
	animation.play()
	constant_linear_velocity = Vector2(0,0)
	
func _process(_delta):
	
	if player_in_area and player_in_area.is_in_group("player") and Input.is_action_just_pressed("interact"):
		if Globals.player.is_inventory_full():
			self.try_to_combine_items()
			if first_potion:
				SignalBus.emit_signal("dialog","first_potion")
				first_potion = false
		else:
			SignalBus.emit_signal("dialog","need_more_items")
	
func try_to_combine_items():
	cauldron_progress_bar.visible = true	
	cauldron_timer.start()
	
func combine(ingredients : Array):
	# Combines resource_nodes to receive a potion resource node
	var result = Globals.item_manager.get_item("potion")
	
	result.effect = ingredients.map(func(i): return i.effect).reduce(func(a,b): return a+b)/ingredients.size()
	
	return result


func _on_cauldron_area_body_entered(body):
	player_in_area = body

func _on_cauldron_area_body_exited(_body):
	player_in_area = null


func _on_timer_timeout():
	var progess = cauldron_progress_bar.value
	
	if progess >= 100:
		cauldron_timer.stop()
		cauldron_progress_bar.visible = false
		cauldron_progress_bar.value = 0
		var items = Globals.player.get_inventory_and_empty()
		var potion = Potion.new()
		potion.icon = load("res://assets/item/potion.png")
		potion.items = items
		Globals.player.give_potion(potion)
		$AudioStreamPlayer2D2.play()
		
		return
	cauldron_progress_bar.value = progess + 5	
