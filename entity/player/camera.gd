extends Camera2D

var zoom_enabled = false
@export var shake_fade: float = 5.0

var rng = RandomNumberGenerator.new()
var shake_strength: float = 0.0
@onready var effect_overlay = $ColorRect

func _ready():
	SignalBus.map_effect_overlay.connect(_change_effect_overlay)
	effect_overlay.color = Color.TRANSPARENT
func _process(delta):
	if shake_strength > 0:
		shake_strength = lerpf(shake_strength,0,shake_fade * delta)
		offset = randomOffset()	
	if zoom_enabled:
		if Input.is_action_just_pressed("zoom_in") and zoom <= Vector2(2,2):
			zoom += Vector2(0.1,0.1)
		if Input.is_action_just_pressed("zoom_out") and zoom >= Vector2(0.3,0.3):
			zoom -= Vector2(0.1,0.1)

func shake(random_strength):
	shake_strength = random_strength
	
func randomOffset() -> Vector2:
	return Vector2(rng.randf_range(-shake_strength,shake_strength),rng.randf_range(-shake_strength,shake_strength))	

func _change_effect_overlay(stage: int):
	match stage:
		0:
			effect_overlay.color = Color.TRANSPARENT
		1:
			effect_overlay.color = Color(Color.hex(0x3B2B4F), 0.3)
		2:
			effect_overlay.color = Color(Color.hex(0x312442), 0.4)	
		3:
			effect_overlay.color = Color(Color.hex(0x271D35), 0.5)
		4:
			effect_overlay.color = Color(Color.hex(0x1E1528), 0.6)
	
