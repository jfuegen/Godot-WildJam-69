extends StaticBody2D


@onready var toggle_interaction = $TextureRect

var MAX_METER = 15
var current_meter = 0
var current_stage = 0
var player_in_area
var first_time = true
var last_potion

func _ready():
	$AnimatedSprite2D.play("default")

func _process(_delta):
	if player_in_area and Input.is_action_just_pressed("interact"):
		if Globals.player.holding_potion:
			last_potion = Globals.player.holding_potion
			var potion = Globals.player.remove_potion()
			SignalBus.add_potion_to_journal.emit(potion)
			if first_time:
				first_time = false
				SignalBus.emit_signal("dialog","first_void")
			_calculate_difficulty()
		else:
			SignalBus.emit_signal("dialog","not_holding_potion")
	if current_meter >= MAX_METER:
		$AnimatedSprite2D.play("die")
		SignalBus.game_over.emit()

func _calculate_difficulty():
	var combined_value = 0
	for item in last_potion.items:
		if !item.effect == -1:
			current_meter += item.effect
		else:
			combined_value -= 1
	if combined_value == -3:
		SignalBus.win_game.emit()
	if current_meter / 3 >= current_stage + 1:
		SignalBus.increase_stage.emit()
		current_stage += 1
	if current_meter / 3 <= current_stage -1:
		SignalBus.decrease_stage.emit()
		if current_meter >= 0:
			current_stage -= 1
	SignalBus.round_over.emit()
	SignalBus.push_current_meter.emit(current_meter, current_stage)

func _on_consumeradius_body_entered(body):
	if body.is_in_group("player"):
		player_in_area = body


func _on_consumeradius_body_exited(body):
	if body.is_in_group("player"):
		player_in_area = null
