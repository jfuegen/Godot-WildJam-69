extends Node

var ui_manager = UIManager.new()
var level_manager = LevelManager.new()
var item_manager = ItemManager.new()
var items

func _ready():
	_add_canvas_layers_to_global()
	_connect_signals()
	_add_managers_to_tree()
	ui_manager.display_start_screen()
	items = item_manager.get_items()

func _add_managers_to_tree():
	add_child(ui_manager)
	add_child(level_manager)
	add_child(item_manager)

func _connect_signals():
	SignalBus.start_game.connect(_start_game)
	SignalBus.round_over.connect(_regenerate_level)

func _add_canvas_layers_to_global():
	Globals.hud_layer = $HUDLayer
	Globals.menu_layer = $MenuLayer
	Globals.overlay_layer = $OverlayLayer
	
func _start_game():
	ui_manager.initiate_hud()
	level_manager.create_and_distribute_item_nodes(item_manager.get_items())
	level_manager.init_all_entities()
	SignalBus.dialog.emit("introduction")
	
func _regenerate_level():
	level_manager.remove_item_nodes()
	level_manager.create_and_distribute_item_nodes(items)
	
