extends Control

func trigger():
	if visible:
		hide()
	else:
		visible = true

func _on_quit_game_button_pressed():
	get_tree().quit()

func _on_continue_button_pressed():
	Globals.menu_layer.remove_child(self)
