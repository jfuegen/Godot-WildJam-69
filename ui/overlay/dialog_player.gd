extends CanvasLayer

var scene_text = {}
var selected_text = []
var in_progress = false
var file = FileAccess.open("res://data/dialog.json",FileAccess.READ)

@onready var background = $TextureRect
@onready var text_label = $Label
@onready var skip = $SkipLabel

func _ready():
	background.visible = false
	skip.visible = false
	scene_text = _load_scene_text()
	SignalBus.dialog.connect(_on_display_dialog)

func _input(event):
	if in_progress and event.is_action_pressed("next_dialog"):
		SignalBus.emit_signal("dialog","introduction")

func _load_scene_text():
	if file:
		var json_text = JSON.parse_string(file.get_as_text())
		return json_text

func _on_display_dialog(text_key):
	get_tree().paused = true
	if in_progress:
		_next_line()
	else:
		background.visible = true
		skip.visible = true
		in_progress = true
		selected_text = scene_text[text_key].duplicate()
		_show_text()
		
func _next_line():
	if selected_text.size() > 0:
		_show_text()
	else:
		_finish()
		
func _show_text():
	text_label.text = selected_text.pop_front()
	
func _finish():
	text_label.text = ""
	background.visible = false
	skip.visible = false
	in_progress = false
	get_tree().paused = false
