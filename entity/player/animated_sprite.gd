extends AnimatedSprite2D

var last_direction = "up"

func _process(_delta):
	var input = Input.get_vector("left","right","up","down")
	if input == Vector2(0,0):
		play("idle_" + last_direction)
	else:
		if input.x > 0:
			last_direction = "right"
			play("walk_right")
		elif input.x < 0:
			last_direction = "left"
			play("walk_left")
		if input.y < 0:
			last_direction = "up"
			play("walk_up")
		elif input.y > 0:
			last_direction = "down"
			play("walk_down")			
