extends Node
class_name UIManager

@onready var start_screen = preload("res://ui/overlay/start_screen.tscn").instantiate()
@onready var victory_screen = preload("res://ui/overlay/victory_screen.tscn").instantiate()
@onready var defeat_screen = preload("res://ui/overlay/defeat_screen.tscn").instantiate()
@onready var pause_menu = preload("res://ui/menu/pause_menu.tscn").instantiate()
@onready var inventory = preload("res://ui/hud/inventory.tscn").instantiate()
@onready var journal = preload("res://ui/menu/journal.tscn").instantiate()
@onready var dev_menu = preload("res://util/dev_ui.tscn").instantiate()
@onready var dialog_player = preload("res://ui/overlay/dialog_player.tscn").instantiate()
@onready var void_gauge = preload("res://ui/hud/void_gauge.tscn").instantiate()
@onready var enter_map_dialog = preload("res://ui/menu/enter_map.tscn").instantiate()
@onready var stage_timer = preload("res://ui/hud/stage_timer.tscn").instantiate()

func _ready():
	SignalBus.enter_map.connect(_ask_to_enter_map)
	SignalBus.add_potion_to_journal.connect(_add_potion_to_journal)
	SignalBus.game_over.connect(_display_gameover_screen)
	SignalBus.win_game.connect(_display_victory_screen)
	_initate_overlay_layer()
	_initiate_menu_layer()

func _initate_overlay_layer():
	Globals.overlay_layer.add_child(start_screen)
	Globals.overlay_layer.add_child(dialog_player)
	Globals.overlay_layer.add_child(victory_screen)
	Globals.overlay_layer.add_child(defeat_screen)

func _initiate_menu_layer():
	Globals.menu_layer.add_child(pause_menu)
	Globals.menu_layer.add_child(dev_menu)
	Globals.menu_layer.add_child(journal)
	Globals.menu_layer.add_child(enter_map_dialog)

func initiate_hud():
	Globals.hud_layer.add_child(inventory)
	Globals.hud_layer.add_child(void_gauge)
	Globals.hud_layer.add_child(stage_timer)

func _add_potion_to_journal(potion: Potion):
	var items = potion.items
	journal.add_to_journal(items)

func display_start_screen():
	start_screen.visible = true

func _display_gameover_screen():
	SignalBus.round_over.emit()
	var timer = Timer.new()
	add_child(timer)
	timer.start(0.5)
	await timer.timeout
	defeat_screen.visible = true
	get_tree().paused = true

func _display_victory_screen():
	SignalBus.round_over.emit()
	var timer = Timer.new()
	add_child(timer)
	timer.start(0.5)
	await timer.timeout
	victory_screen.visible = true
	get_tree().paused = true

func _input(event):
	if event.is_action_pressed("pause_menu"):
		pause_menu.trigger()
	if event.is_action_pressed("dev"):
		dev_menu.trigger()
	if event.is_action_pressed("journal"):
		journal.trigger()

func _ask_to_enter_map():
	enter_map_dialog.trigger()
