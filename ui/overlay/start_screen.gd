extends Control


func _on_start_game_pressed():
	SignalBus.start_game.emit()
	self.queue_free()
