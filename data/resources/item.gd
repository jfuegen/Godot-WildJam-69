extends Resource

class_name Item

@export var title : String
@export var description : String
@export var icon : Texture2D
@export var effect : int
