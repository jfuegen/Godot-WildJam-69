extends Control

func _ready():
	SignalBus.push_to_inventory.connect(_display_items)

func _display_items(items: Array):
	var slots = $HBoxContainer.get_children()
	for item in items:
		if slots:
			slots.pop_front().texture = item.icon
	if !items:
		for slot in slots:
			slot.texture = null
