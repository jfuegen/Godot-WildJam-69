extends Control

@onready var timer = $Timer as Timer
@onready var label = $Label as Label

func _ready():
	SignalBus.entering_map.connect(_entering_map)
	SignalBus.returned_to_spawn.connect(_returned)

func _process(_delta):
	label.text = "%d:%02d" % [floor(timer.time_left / 60), int(timer.time_left) % 60]
	if timer.time_left < 10:
		label.label_settings.font_color = Color.FIREBRICK
	else:
		label.label_settings.font_color = Color.WHITE
func _entering_map():
	timer.start(30)

func _returned():
	timer.stop()

func _on_timer_timeout():
	SignalBus.game_over.emit()
