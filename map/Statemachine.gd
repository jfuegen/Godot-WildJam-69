extends Node
class_name Statemachine

var state_dict :Dictionary 
var current_state :State
var map: TileMap

func _init(tile_map: TileMap):
	map = tile_map
	_create_states()
	SignalBus.increase_stage.connect(next_state)
	SignalBus.decrease_stage.connect(previous_state)
	
func _create_states():
	for i in range(5):
		if i == 0:
			state_dict[i] = State.new(null,null,null)
		elif i == 4:
			state_dict[i] = State.new(i+2,null,null)
		else:
			state_dict[i] = State.new(i+2,null,null)
		state_dict[i].map = map
	
	for i in range(5):
		if i == 0:
			state_dict[i].set_states(state_dict[i+1],null)
		elif i == 4:
			state_dict[i].set_states(null,state_dict[i-1])
		else:
			state_dict[i].set_states(state_dict[i+1],state_dict[i-1])
	
	current_state = state_dict[0]
	current_state.enter_state()

func next_state():
	if !current_state.voidlayer == 6 :
		current_state = current_state.previous_state
		current_state.enter_state()
		SignalBus.map_effect_overlay.emit(current_state.voidlayer - 2)
#	

func previous_state():
	var oldstate = current_state
	
	if !current_state.voidlayer == null:		
		current_state = oldstate.next_state
		oldstate.exit_state()
		current_state.enter_state()
		if current_state.voidlayer:
			SignalBus.map_effect_overlay.emit(current_state.voidlayer - 2)
		else:
			SignalBus.map_effect_overlay.emit(0)

