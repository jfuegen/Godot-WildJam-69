extends CharacterBody2D

var speed = 200
var picking_ressource = false
var picking_item
var picking_ressource_reference
var holding_potion
var inventory: Array

func _ready():
	Globals.player = self	
	$PickingRessourceProgress.hide()

func _physics_process(delta):
	if picking_ressource: # Don't move while picking ressources
		return
	var input = Input.get_vector("left","right","up","down")
	velocity = input * (speed + delta)
	move_and_slide()


func try_to_give_item(item: Item, node):
	picking_ressource = true
	$PickingRessourceProgress.visible = true	
	$PickingRessourceTimer.start()	
	picking_item = item
	picking_ressource_reference = node

func _on_picking_ressource_timer_timeout():
	var progess = $PickingRessourceProgress.value 
	if progess >= 100:
		$PickingRessourceTimer.stop()
		$PickingRessourceProgress.visible = false
		$PickingRessourceProgress.value = 0
		picking_ressource = false
		inventory.append(picking_item)
		SignalBus.push_to_inventory.emit(inventory)
		picking_ressource_reference.queue_free()
		return
	$PickingRessourceProgress.value = progess + 5		

func give_potion(potion: Potion):
	holding_potion = potion
	$Potion.texture = potion.icon
	$Potion.position = Vector2(0,-32)

func remove_potion() -> Potion:
	var temp_potion = holding_potion
	holding_potion = null
	$Potion.texture = null
	return temp_potion

func get_inventory_and_empty() -> Array:
	var inventory_snapshot = inventory
	inventory = []
	SignalBus.push_to_inventory.emit(inventory)
	return inventory_snapshot

func is_inventory_full() -> bool:
	if inventory.size() < 3:
		return false
	else:
		return true
