extends Control

@onready var background = $TextureRect
@onready var body = $ScrollContainer
@onready var potion_container = $ScrollContainer/HFlowContainer

func _ready():
	hide()

func trigger():
	if visible:
		hide()
	else:
		visible = true

func add_to_journal(potion: Array):
	var container = VFlowContainer.new()
	var potion_grade = VFlowContainer.new()
	var grades = []
	for item in potion:
		var item_display = TextureRect.new()
		item_display.texture = item.icon
		container.add_child(item_display)
		if item.effect == 1:
			var text = TextureRect.new()
			text.texture = load("res://assets/ui/journal/bad.png")
			grades.append(text)
		if item.effect == 0:
			var text = TextureRect.new()
			text.texture = load("res://assets/ui/journal/neutral.png")
			grades.append(text)
		if item.effect == -1:
			var text = TextureRect.new()
			text.texture = load("res://assets/ui/journal/good.png")
			grades.append(text)
	grades.shuffle()
	for grade in grades:
		potion_grade.add_child(grade)
	container.add_child(potion_grade)
	potion_container.add_child(container)
