extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	SignalBus.push_current_meter.connect(_change_meter_value)

func _change_meter_value(meter: int, stage: int):
	$TextureProgressBar.value = meter
	$Label.text = str(stage)
