extends Node2D
class_name ItemManager

var cache : Dictionary = {}
var item_folder = 'res://item/item_ressources/'

func _ready():
	self.cache = self.load_items(item_folder)
	
func valid(items : Array[Item]) -> bool:
	var ids = items.map(func(item): return item.id)
	var unique = Array()
	
	for id in ids:
		if id not in unique:
			unique.append(id)
	
	return unique.size() == ids.size()

func load_items(folder_name : String) -> Dictionary:
	# load all items resources into a dictionary
	var collection = Dictionary()
	var folder = DirAccess.open(folder_name)
	
	folder.list_dir_begin()
	var file_name = folder.get_next()
	# loop through all files in directory and add to items
	while file_name != "":
		if file_name.ends_with(".tres.remap"):
			file_name = file_name.replace(".remap", "")
		if file_name.ends_with(".tres"):
			collection[file_name] = load(folder_name + '/' + file_name)
			file_name = folder.get_next()
		#collection[file_name] = load(folder_name + "/" + file_name)
		#file_name = folder.get_next()
	return collection


func get_item(id) -> Item:
	return cache[id + ".tres"]
	
func get_items() -> Array: 
	var effects = [-1,-1,-1,0,0,0,1,1,1,1,1,1,1,1,1]
	effects.shuffle()
	for item in cache.values():
		item.effect = effects.pop_front()
	return cache.values()

		

