extends Node2D

var item: Item
var player_in_area = null

func _process(_delta):
	var inventory_full = Globals.player.is_inventory_full()
	if player_in_area and player_in_area.is_in_group("player") and Input.is_action_just_pressed("interact") and not inventory_full:
		player_in_area.try_to_give_item(item,self)

func set_properties(i: Item):
	item = i
	var sprite = Sprite2D.new()
	sprite.texture = i.icon
	add_child(sprite)
	$Area2D/CollisionShape2D.shape.size = Vector2(32,32)

func get_area2d() -> Area2D:
	return $Area2D

func get_item() -> Item:
	return item

func _on_area_2d_body_entered(body):
	player_in_area = body

func _on_area_2d_body_exited(_body):
	player_in_area = null
