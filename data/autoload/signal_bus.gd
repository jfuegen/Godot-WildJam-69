extends Node

# Calls the DialogPlayer and plays the provided Dialog
signal dialog(text_key)

# Tells the game to unpause and display relevant scenes
signal start_game

# Tells the game to redistribute ItemNodes and reinstantiate the map
signal round_over

# Pushes the current player inventory to the HUD
signal push_to_inventory(items: Array)

# Pushed current meter to VoidGauge
signal push_current_meter(meter: int, stage:int)

# Adds the potion given to the VoidEntity to the Journal
signal add_potion_to_journal(potion)

# Signals that the Stage has inreased
signal increase_stage

# Signals that the Stage has decreased
signal decrease_stage

# Tells the camera to draw a new Overlay depending on the stage
signal map_effect_overlay(current_stage)

# Emitted when the player enters the collisionarea asking to enter the map
signal enter_map

# Emitted when the player enters the map
signal entering_map

# Emitted when the player returns to spawn from the map
signal returned_to_spawn

# Emitted when the the game is won
signal win_game

# Emitted when the player looses the game
signal game_over
