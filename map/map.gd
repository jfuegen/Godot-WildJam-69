extends Node2D

@onready var tile_map = $TileMap
@onready var audio_brewing = $AudioBrewingArea
@onready var audio_gathering = $AudioGatheringArea
var state_machine
var has_entered = false

func _ready():
	SignalBus.entering_map.connect(_entered_map)
	state_machine = Statemachine.new(tile_map)

func distribute_item_nodes(nodes: Array):
	var used_cells = tile_map.get_used_cells(0)
	for location in used_cells:
		var node = nodes.pop_front()
		if node:
			node.position = tile_map.map_to_local(location)
			add_child(node)

# Removes all item nodes from map, theres is a good reason to do it this way, trust.
func remove_item_nodes():
	var children = get_children()
	for child in children:
		if child.has_method("set_properties"):
			remove_child(child)
	
func init_player(player):
	player.position = Vector2(-16,400)
	add_child(player)
	audio_brewing.play()

func init_void_entity(void_entity):
	void_entity.position = Vector2(140,400)
	add_child(void_entity)

func init_cauldron(cauldron):
	cauldron.position = Vector2(17,490)
	add_child(cauldron)

func _on_enter_map_body_entered(body):
	if body.is_in_group("player"):
		if !has_entered:
			SignalBus.enter_map.emit()
		else:
			if Globals.player.inventory.size() < 3:
				SignalBus.dialog.emit("need_more_items")
				Globals.player.position -= Vector2(0,30)
			else:
				SignalBus.returned_to_spawn.emit()
				$Wall.visible = true
				$Wall.collision_layer = 1
				has_entered = false
				audio_brewing.play()
				audio_gathering.stop()

func _entered_map():
	has_entered = true
	$Wall.hide()
	$Wall.collision_layer = 10
	audio_brewing.stop()
	audio_gathering.play()

