extends ColorRect
class_name Fader

var transition: String
var fading: String

# Called when the node enters the scene tree for the first time.
func _ready():
	color = Color(Color.BLACK, 0)
	SignalBus.increase_stage.connect(_set_bad_transition)
	SignalBus.decrease_stage.connect(_set_good_transition)
	SignalBus.round_over.connect(_trigger_transition)

func _process(_delta):
	if fading == "in" and color.a < 1:
		color.a += 0.01
		if color.a >= 1:
			fading = "out"
	elif fading == "out" and color.a > 0:
		color.a -= 0.01

func _set_bad_transition():
	transition = "bad"

func _set_good_transition():
	transition = "good"
	
func _trigger_transition():
	fading = "in"
	
