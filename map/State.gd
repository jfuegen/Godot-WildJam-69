extends Node
class_name State

var voidlayer
var previous_state = null
var next_state = null
var map

func _init(layer,last_state,nex_state):
	voidlayer = layer
	self.next_state = nex_state
	previous_state = last_state
	
func enter_state() -> void:
	if voidlayer != null:
		map.set_layer_enabled(voidlayer,true)
	
func exit_state() -> void:
	if voidlayer != null:
		map.set_layer_enabled(voidlayer,false)

func _remove_current_void_layer() -> void:
	map.set_layer_enabled(voidlayer,false)

func set_states(wstate :State,bstate :State):
	previous_state = wstate
	next_state = bstate


	
	
	
		
